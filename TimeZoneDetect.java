import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A Java implementation of Jon Nylander's jsTimezoneDetect library, intended for use in GWT applications.
 * 
 * @see <a href="https://bitbucket.org/pellepim/jstimezonedetect/overview">jsTimezoneDetect on BitBucket</a>
 * @see <a href="https://bitbucket.org/pellepim/jstimezonedetect/src/f9e3e30e1e1f53dd27cd0f73eb51a7e7caf7b378/jstz.js?at=default">jsTimezoneDetect 1.0.5 code (basis for this code)</a>
 * @author jeffevans@gaggle.net
 */
// ignore java.util.Date deprecation warnings (in GWT, preferred java.util.Calendar is not implemented)
@SuppressWarnings("deprecation")
public abstract class TimeZoneDetect {
    private static final String HEMISPHERE_SOUTH = "s";

    /**
     * The keys in this object are timezones that we know may be ambiguous after
     * a preliminary scan through the olson_tz object.
     *
     * The array of timezones to compare must be in the order that daylight savings
     * starts for the regions.
     */
    private static final Map<String, List<String>> AMBIGUITIES = new HashMap<String, List<String>>();

    /**
     * This object contains information on when daylight savings starts for
     * different timezones.
     * 
     * The list is short for a reason. Often we do not have to be very specific
     * to single out the correct timezone. But when we do, this list comes in
     * handy.
     *
     * Each value is a date denoting when daylight savings starts for that timezone.
     */
    private static final Map<String, Date> DST_STARTS = new HashMap<String, Date>();
    
    /**
     * The keys in this dictionary are comma separated as such:
     * 
     * First the offset compared to UTC time in minutes.
     * 
     * Then a flag which is 0 if the timezone does not take daylight savings
     * into account and 1 if it does.
     * 
     * Thirdly an optional 's' signifies that the timezone is in the southern
     * hemisphere, only interesting for timezones with DST.
     * 
     * The mapped arrays is used for constructing the jstz.TimeZone object from
     * within jstz.determine_timezone();
     */
    private static final Map<String, String> OLSON_TIMEZONES = new HashMap<String, String>();
    
    private static final Date RU_PRE_DST_CHANGE = new Date(2010, 6, 15, 1, 0, 0);
    
    /**
     * Gets the offset in minutes from UTC for a certain date.
     * @param date
     * @return the offset, in minutes
     */
    private static int getDateOffset(final Date date) {
        return -date.getTimezoneOffset();
    }

    private static Date getDate(final Integer year, final int month, final int date) {
        final Date returnDate = new Date();
        if (year != null) {
            returnDate.setYear(year);
        }
        returnDate.setMonth(month);
        returnDate.setDate(date);
        
        return returnDate;
    }
    
    private static int getJanuaryOffset(final Integer year) {
        return getDateOffset(getDate(year, 0, 2));
    }

    private static int getJuneOffset(final Integer year) {
        return getDateOffset(getDate(year, 5, 2));
    }
    
    /**
     * Private method.
     * Checks whether a given date is in daylight saving time.
     * If the date supplied is after august, we assume that we're checking
     * for southern hemisphere DST.
     * @param date
     * @return boolean indicating whether a given date is in daylight saving time
     */
    private static boolean dateIsDst(final Date date) {
        final boolean southern = date.getMonth() > 7;
        final int baseOffset = southern ? getJuneOffset(date.getYear()) : 
            getJanuaryOffset(date.getYear());
        final int dateOffset = getDateOffset(date);
        final boolean west = baseOffset < 0;
        final int dstOffset = baseOffset - dateOffset;
            
        if (!west && !southern) {
            return dstOffset < 0;
        }
        
        return dstOffset != 0;
    }
    
    /**
     * This function does some basic calculations to create information about
     * the user's timezone. It uses REFERENCE_YEAR as a solid year for which
     * the script has been tested rather than depend on the year set by the
     * client device.
     * 
     * @return a key that can be used to do lookups in OLSON_TIMEZONES.
     * eg: "720,1,2". 
     */
    private static String lookupKey() {
        final int januaryOffset = getJanuaryOffset(null),
            juneOffset = getJuneOffset(null),
            diff = januaryOffset - juneOffset;

        if (diff < 0) {
            return januaryOffset + ",1";
        } else if (diff > 0) {
            return juneOffset + ",1," + HEMISPHERE_SOUTH;
        }

        return januaryOffset + ",0";
    }

    /**
     * Uses lookupKey() to formulate a key to use in the OLSON_TIMEZONES
     * dictionary, and constructs a {@link TimeZone} instance with the result
     * 
     * @returns an instance of {@link TimeZone} object with the timeZoneName
     *          field set and disambiguated
     */
    public static TimeZone determine() {
        final String key = lookupKey();
        return new TimeZone(OLSON_TIMEZONES.get(key));
    }
    
    /**
     * A class that encapsulates the calculation of a time zone name from a
     * constructor argument (starting time zone name) and checking ambiguities
     */
    public static class TimeZone {
        private final String timeZoneName;
        
        public String getTimeZoneName() {
            return timeZoneName;
        }

        private String tempTzName;

        /**
         * Checks if it is possible that the timezone is ambiguous.
         */
        private boolean isAmbiguous() {
            return AMBIGUITIES.containsKey(tempTzName);
        }

        /**
         * Checks if a timezone has possible ambiguities. I.e timezones that are similar.
         *
         * For example, if the preliminary scan determines that we're in America/Denver.
         * We double check here that we're really there and not in America/Mazatlan.
         *
         * This is done by checking known dates for when daylight savings start for different
         * timezones during 2010 and 2011.
         */
        private void ambiguityCheck() {
            final List<String> ambiguityList = AMBIGUITIES.get(tempTzName);

            for (int i=0; i < ambiguityList.size(); i++) {
                final String tz = ambiguityList.get(i);

                if (dateIsDst(DST_STARTS.get(tz))) {
                    tempTzName = tz;
                    return;
                }
            }
        }
        
        public TimeZone(final String tzName) {
            tempTzName = tzName;
            
            if (isAmbiguous()) {
                ambiguityCheck();
            }            
            
            timeZoneName = tempTzName;
        }
    }
    
    static {
        OLSON_TIMEZONES.put("-720,0"   , "Pacific/Majuro");
        OLSON_TIMEZONES.put("-660,0"   , "Pacific/Pago_Pago");
        OLSON_TIMEZONES.put("-600,1"   , "America/Adak");
        OLSON_TIMEZONES.put("-600,0"   , "Pacific/Honolulu");
        OLSON_TIMEZONES.put("-570,0"   , "Pacific/Marquesas");
        OLSON_TIMEZONES.put("-540,0"   , "Pacific/Gambier");
        OLSON_TIMEZONES.put("-540,1"   , "America/Anchorage");
        OLSON_TIMEZONES.put("-480,1"   , "America/Los_Angeles");
        OLSON_TIMEZONES.put("-480,0"   , "Pacific/Pitcairn");
        OLSON_TIMEZONES.put("-420,0"   , "America/Phoenix");
        OLSON_TIMEZONES.put("-420,1"   , "America/Denver");
        OLSON_TIMEZONES.put("-360,0"   , "America/Guatemala");
        OLSON_TIMEZONES.put("-360,1"   , "America/Chicago");
        OLSON_TIMEZONES.put("-360,1,s" , "Pacific/Easter");
        OLSON_TIMEZONES.put("-300,0"   , "America/Bogota");
        OLSON_TIMEZONES.put("-300,1"   , "America/New_York");
        OLSON_TIMEZONES.put("-270,0"   , "America/Caracas");
        OLSON_TIMEZONES.put("-240,1"   , "America/Halifax");
        OLSON_TIMEZONES.put("-240,0"   , "America/Santo_Domingo");
        OLSON_TIMEZONES.put("-240,1,s" , "America/Santiago");
        OLSON_TIMEZONES.put("-210,1"   , "America/St_Johns");
        OLSON_TIMEZONES.put("-180,1"   , "America/Godthab");
        OLSON_TIMEZONES.put("-180,0"   , "America/Argentina/Buenos_Aires");
        OLSON_TIMEZONES.put("-180,1,s" , "America/Montevideo");
        OLSON_TIMEZONES.put("-120,0"   , "America/Noronha");
        OLSON_TIMEZONES.put("-120,1"   , "America/Noronha");
        OLSON_TIMEZONES.put("-60,1"    , "Atlantic/Azores");
        OLSON_TIMEZONES.put("-60,0"    , "Atlantic/Cape_Verde");
        OLSON_TIMEZONES.put("0,0"      , "UTC");
        OLSON_TIMEZONES.put("0,1"      , "Europe/London");
        OLSON_TIMEZONES.put("60,1"     , "Europe/Berlin");
        OLSON_TIMEZONES.put("60,0"     , "Africa/Lagos");
        OLSON_TIMEZONES.put("60,1,s"   , "Africa/Windhoek");
        OLSON_TIMEZONES.put("120,1"    , "Asia/Beirut");
        OLSON_TIMEZONES.put("120,0"    , "Africa/Johannesburg");
        OLSON_TIMEZONES.put("180,0"    , "Asia/Baghdad");
        OLSON_TIMEZONES.put("180,1"    , "Europe/Moscow");
        OLSON_TIMEZONES.put("210,1"    , "Asia/Tehran");
        OLSON_TIMEZONES.put("240,0"    , "Asia/Dubai");
        OLSON_TIMEZONES.put("240,1"    , "Asia/Baku");
        OLSON_TIMEZONES.put("270,0"    , "Asia/Kabul");
        OLSON_TIMEZONES.put("300,1"    , "Asia/Yekaterinburg");
        OLSON_TIMEZONES.put("300,0"    , "Asia/Karachi");
        OLSON_TIMEZONES.put("330,0"    , "Asia/Kolkata");
        OLSON_TIMEZONES.put("345,0"    , "Asia/Kathmandu");
        OLSON_TIMEZONES.put("360,0"    , "Asia/Dhaka");
        OLSON_TIMEZONES.put("360,1"    , "Asia/Omsk");
        OLSON_TIMEZONES.put("390,0"    , "Asia/Rangoon");
        OLSON_TIMEZONES.put("420,1"    , "Asia/Krasnoyarsk");
        OLSON_TIMEZONES.put("420,0"    , "Asia/Jakarta");
        OLSON_TIMEZONES.put("480,0"    , "Asia/Shanghai");
        OLSON_TIMEZONES.put("480,1"    , "Asia/Irkutsk");
        OLSON_TIMEZONES.put("525,0"    , "Australia/Eucla");
        OLSON_TIMEZONES.put("525,1,s"  , "Australia/Eucla");
        OLSON_TIMEZONES.put("540,1"    , "Asia/Yakutsk");
        OLSON_TIMEZONES.put("540,0"    , "Asia/Tokyo");
        OLSON_TIMEZONES.put("570,0"    , "Australia/Darwin");
        OLSON_TIMEZONES.put("570,1,s"  , "Australia/Adelaide");
        OLSON_TIMEZONES.put("600,0"    , "Australia/Brisbane");
        OLSON_TIMEZONES.put("600,1"    , "Asia/Vladivostok");
        OLSON_TIMEZONES.put("600,1,s"  , "Australia/Sydney");
        OLSON_TIMEZONES.put("630,1,s"  , "Australia/Lord_Howe");
        OLSON_TIMEZONES.put("660,1"    , "Asia/Kamchatka");
        OLSON_TIMEZONES.put("660,0"    , "Pacific/Noumea");
        OLSON_TIMEZONES.put("690,0"    , "Pacific/Norfolk");
        OLSON_TIMEZONES.put("720,1,s"  , "Pacific/Auckland");
        OLSON_TIMEZONES.put("720,0"    , "Pacific/Tarawa");
        OLSON_TIMEZONES.put("765,1,s"  , "Pacific/Chatham");
        OLSON_TIMEZONES.put("780,0"    , "Pacific/Tongatapu");
        OLSON_TIMEZONES.put("780,1,s"  , "Pacific/Apia");
        OLSON_TIMEZONES.put("840,0"    , "Pacific/Kiritimati");

        AMBIGUITIES.put("America/Denver", Arrays.asList("America/Denver", "America/Mazatlan"));
        AMBIGUITIES.put("America/Chicago", Arrays.asList("America/Chicago", "America/Mexico_City"));
        AMBIGUITIES.put("America/Santiago", Arrays.asList("America/Santiago", "America/Asuncion", "America/Campo_Grande"));
        AMBIGUITIES.put("America/Montevideo", Arrays.asList("America/Montevideo", "America/Sao_Paulo"));
        AMBIGUITIES.put("Asia/Beirut", Arrays.asList("Asia/Amman", "Asia/Jerusalem", "Asia/Beirut", "Europe/Helsinki", "Asia/Damascus"));
        AMBIGUITIES.put("Pacific/Auckland", Arrays.asList("Pacific/Auckland", "Pacific/Fiji"));
        AMBIGUITIES.put("America/Los_Angeles", Arrays.asList("America/Los_Angeles", "America/Santa_Isabel"));
        AMBIGUITIES.put("America/New_York", Arrays.asList("America/Havana", "America/New_York"));
        AMBIGUITIES.put("America/Halifax", Arrays.asList("America/Goose_Bay", "America/Halifax"));
        AMBIGUITIES.put("America/Godthab", Arrays.asList("America/Miquelon", "America/Godthab"));
        AMBIGUITIES.put("Asia/Dubai", Arrays.asList("Europe/Moscow"));
        AMBIGUITIES.put("Asia/Dhaka", Arrays.asList("Asia/Yekaterinburg"));
        AMBIGUITIES.put("Asia/Jakarta", Arrays.asList("Asia/Omsk"));
        AMBIGUITIES.put("Asia/Shanghai", Arrays.asList("Asia/Krasnoyarsk", "Australia/Perth"));
        AMBIGUITIES.put("Asia/Tokyo", Arrays.asList("Asia/Irkutsk"));
        AMBIGUITIES.put("Australia/Brisbane", Arrays.asList("Asia/Yakutsk"));
        AMBIGUITIES.put("Pacific/Noumea", Arrays.asList("Asia/Vladivostok"));
        AMBIGUITIES.put("Pacific/Tarawa", Arrays.asList("Asia/Kamchatka", "Pacific/Fiji"));
        AMBIGUITIES.put("Pacific/Tongatapu", Arrays.asList("Pacific/Apia"));
        AMBIGUITIES.put("Asia/Baghdad", Arrays.asList("Europe/Minsk"));
        AMBIGUITIES.put("Asia/Baku", Arrays.asList("Asia/Yerevan", "Asia/Baku"));
        AMBIGUITIES.put("Africa/Johannesburg", Arrays.asList("Asia/Gaza", "Africa/Cairo"));

        DST_STARTS.put("America/Denver", new Date(2011, 2, 13, 3, 0, 0));
        DST_STARTS.put("America/Mazatlan", new Date(2011, 3, 3, 3, 0, 0));
        DST_STARTS.put("America/Chicago", new Date(2011, 2, 13, 3, 0, 0));
        DST_STARTS.put("America/Mexico_City", new Date(2011, 3, 3, 3, 0, 0));
        DST_STARTS.put("America/Asuncion", new Date(2012, 9, 7, 3, 0, 0));
        DST_STARTS.put("America/Santiago", new Date(2012, 9, 3, 3, 0, 0));
        DST_STARTS.put("America/Campo_Grande", new Date(2012, 9, 21, 5, 0, 0));
        DST_STARTS.put("America/Montevideo", new Date(2011, 9, 2, 3, 0, 0));
        DST_STARTS.put("America/Sao_Paulo", new Date(2011, 9, 16, 5, 0, 0));
        DST_STARTS.put("America/Los_Angeles", new Date(2011, 2, 13, 8, 0, 0));
        DST_STARTS.put("America/Santa_Isabel", new Date(2011, 3, 5, 8, 0, 0));
        DST_STARTS.put("America/Havana", new Date(2012, 2, 10, 2, 0, 0));
        DST_STARTS.put("America/New_York", new Date(2012, 2, 10, 7, 0, 0));
        DST_STARTS.put("Europe/Helsinki", new Date(2013, 2, 31, 5, 0, 0));
        DST_STARTS.put("Pacific/Auckland", new Date(2011, 8, 26, 7, 0, 0));
        DST_STARTS.put("America/Halifax", new Date(2011, 2, 13, 6, 0, 0));
        DST_STARTS.put("America/Goose_Bay", new Date(2011, 2, 13, 2, 1, 0));
        DST_STARTS.put("America/Miquelon", new Date(2011, 2, 13, 5, 0, 0));
        DST_STARTS.put("America/Godthab", new Date(2011, 2, 27, 1, 0, 0));
        DST_STARTS.put("Europe/Moscow", RU_PRE_DST_CHANGE);
        DST_STARTS.put("Asia/Amman", new Date(2013, 2, 29, 1, 0, 0));
        DST_STARTS.put("Asia/Beirut", new Date(2013, 2, 31, 2, 0, 0));
        DST_STARTS.put("Asia/Damascus", new Date(2013, 3, 6, 2, 0, 0));
        DST_STARTS.put("Asia/Jerusalem", new Date(2013, 2, 29, 5, 0, 0));
        DST_STARTS.put("Asia/Yekaterinburg", RU_PRE_DST_CHANGE);
        DST_STARTS.put("Asia/Omsk", RU_PRE_DST_CHANGE);
        DST_STARTS.put("Asia/Krasnoyarsk", RU_PRE_DST_CHANGE);
        DST_STARTS.put("Asia/Irkutsk", RU_PRE_DST_CHANGE);
        DST_STARTS.put("Asia/Yakutsk", RU_PRE_DST_CHANGE);
        DST_STARTS.put("Asia/Vladivostok", RU_PRE_DST_CHANGE);
        DST_STARTS.put("Asia/Baku", new Date(2013, 2, 31, 4, 0, 0));
        DST_STARTS.put("Asia/Yerevan", new Date(2013, 2, 31, 3, 0, 0));
        DST_STARTS.put("Asia/Kamchatka", RU_PRE_DST_CHANGE);
        DST_STARTS.put("Asia/Gaza", new Date(2010, 2, 27, 4, 0, 0));
        DST_STARTS.put("Africa/Cairo", new Date(2010, 4, 1, 3, 0, 0));
        DST_STARTS.put("Europe/Minsk", RU_PRE_DST_CHANGE);
        DST_STARTS.put("Pacific/Apia", new Date(2010, 10, 1, 1, 0, 0));
        DST_STARTS.put("Pacific/Fiji", new Date(2010, 11, 1, 0, 0, 0));
        DST_STARTS.put("Australia/Perth", new Date(2008, 10, 1, 1, 0, 0));
    }
}
